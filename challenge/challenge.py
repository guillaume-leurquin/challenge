"""Predict movie genres based on the synopsis.

Defines a flask application that can receive train data to train a
model to predict movie genres based on their synopsis.
"""

from io import StringIO

import flask
import nltk
import numpy as np
import pandas as pd
import regex as re
import sklearn
import sklearn.calibration
import sklearn.feature_extraction
import sklearn.multiclass
import sklearn.preprocessing


def _strip_accents(text):
    if "ø" in text or "Ø" in text:
        # Do nothing when finding ø
        return text
    return str(text.encode("ascii", "ignore").decode("utf-8"))


def prepare_nltk():
    """Download nltk dependencies."""
    nltk.download("punkt")
    nltk.download("wordnet")
    nltk.download("stopwords")


class Preprocessor:
    """Preprocess text using nltk."""

    def __init__(self):
        self.stemmer = nltk.stem.PorterStemmer()
        self.stop_words = (
            set(nltk.corpus.stopwords.words("english"))
            .union(["``", "''", "'s", "'ll", "n't", "'re", "'ve", "'the"])
        )

    def _preprocess_text(self, document):
        document = _strip_accents(document).lower()

        # Remove all the special characters
        document = re.sub(r"[\W\d]", " ", document)

        # remove all single characters
        document = re.sub(r"\s+[a-zA-Z]\s+", " ", document)

        # Remove single characters from the start
        document = re.sub(r"\^[a-zA-Z]\s+", " ", document)

        # Substituting multiple spaces with single space
        document = re.sub(r"\s+", " ", document, flags=re.I)

        # Tokenization
        tokens = nltk.word_tokenize(document)
        return " ".join(
            self.stemmer.stem(word)
            for word in tokens if word not in self.stop_words
        )

    def __call__(self, document):
        """Stem the sentences in the document.

        Uses nltk's word tokenizer and stemmer. Removes stop words.
        """
        return self._preprocess_text(document)


class MovieGenreModel:
    """Can predict movie genres based on a synopsis."""

    def __init__(self):
        prepare_nltk()

        classifier = sklearn.calibration.CalibratedClassifierCV(
            sklearn.svm.LinearSVC(
                loss="squared_hinge",
                random_state=42,
                C=0.2,
                tol=1e-7,
                dual=True,
                verbose=20,
            )
        )

        self.vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(
            lowercase=False,
            strip_accents=None,
            analyzer="word",
            stop_words=None,
            ngram_range=(1, 5),
            max_df=0.7,
            min_df=2,
            max_features=None,
            use_idf=True,
            smooth_idf=True,
            sublinear_tf=True,
        )

        self.clf = sklearn.multiclass.OneVsRestClassifier(
            classifier,
            n_jobs=-1
        )

        self.label_encoder = sklearn.preprocessing.MultiLabelBinarizer()

        self.preprocessor = Preprocessor()

    def _prepare_train_data(self, train_data):
        preprocessed = train_data["synopsis"].apply(self.preprocessor)
        train_features = self.vectorizer.fit_transform(preprocessed)
        train_labels = self.label_encoder.fit_transform(
            train_data["genres"].str.split(" ")
        ).tolist()

        return train_features, train_labels

    def train(self, train_data):
        """Train the model.

        train_data: pandas dataframe with the synopsis and genres columns.
        """
        train_features, train_labels = self._prepare_train_data(train_data)
        self.clf.fit(train_features, train_labels)

    def predict(self, test_data, k=5):
        """Predict using the trained model.

        test_data: pandas dataframe with the synopsis column.
        k: the k-best movie genres to return.

        Returns
        -------
        The predicted labels, in decreasing order of likelyhood.
        The predicted probabilities.
        """
        preprocessed = test_data["synopsis"].apply(self.preprocessor)
        features = self.vectorizer.transform(preprocessed)

        predictions = self.clf.predict_proba(features)
        top_k_idx = np.argsort(predictions)[:, -k:][:, ::-1]
        predicted_genres = [
            " ".join([self.label_encoder.classes_[idx] for idx in indices])
            for indices in top_k_idx
        ]
        return test_data.assign(predicted_genres=predicted_genres)


app = flask.Flask(__name__)

movie_genre_model = MovieGenreModel()


@app.route("/genres/train", methods=["POST"])
def genres_train():
    """Train the movie genre model.

    This endpoint receives a CSV with the columns
    `movie_id`, `year`, `synopsis`, and `genres`.
    The latter is a space-separated list of
    at most 5 genres that apply to the movie.
    """
    df = pd.read_csv(StringIO(flask.request.data.decode("utf8")))
    print(f"Received training dataset with {len(df)} samples.")
    print("Starting training...")
    movie_genre_model.train(df)
    return "The model was trained successfully"


@app.route("/genres/predict", methods=["POST"])
def genres_predict():
    """Predict genres of movies from their synopsis.

    Given a CSV file containing movies and their synopses, This produces a
    new CSV file. This file has a header and contains 2 columns:

    movie_id is obtained from the input CSV file.
    predicted_genres is a space-separated
    list of exactly 5 movie genres, ordered by
    descending probability
    (i.e. the genre with the highest probability is first).
    """
    input_data = pd.read_csv(StringIO(flask.request.data.decode("utf8")))
    prediction_df = movie_genre_model.predict(input_data)

    prediction_selection = prediction_df[["movie_id", "predicted_genres"]]
    csv_output = prediction_selection.to_csv(index=False)

    response = flask.make_response(csv_output, 200)
    response.mimetype = "text/csv"
    return response
