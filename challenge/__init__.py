"""Init file for the Radix Challenge."""

from .challenge import app

__all__ = ["app"]
